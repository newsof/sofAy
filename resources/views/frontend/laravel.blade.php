<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{env('APP_NAME')}}</title>
        <base href="{{ url('') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <my-app>Loading...</my-app>

        <script type="text/javascript" src="{{ asset('dist/inline.bundle.js') }}?time={{ time() }}">"></script>
        <script type="text/javascript" src="{{ asset('dist/polyfills.bundle.js') }}?time={{ time() }}">"></script>
        <script type="text/javascript" src="{{ asset('dist/styles.bundle.js') }}?time={{ time() }}">"></script>
        <script type="text/javascript" src="{{ asset('dist/vendor.bundle.js') }}?time={{ time() }}">"></script>
        <script type="text/javascript" src="{{ asset('dist/main.bundle.js') }}?time={{ time() }}">"></script>
    </body>
</html>
