
<section id="promotionCat" class="container " >
    <div class="title-category" >
        <strong><b>Promotion</b></strong> <!---->
        <a class="btn btn-info btn-sm viewplus" href="" style="color: white;float: right">Voir plus »<i class="iconf-right"></i></a>
    </div>

    <div class="products-list" >
        <div class="container">
            <div class="row" style="margin: 0;">
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a data-toggle="modal" data-target="#noticeModal">
                                <div style="padding: 10px;">
                                        <span class="label label-danger shipping"
                                              style="font-size: 14px;border-radius: 5px;">-15% </span>
                                </div>
                                <img class="img"
                                     src="http://cdn.shopify.com/s/files/1/1118/2334/products/Depositphotos_14430777_original_60bf1032-d69e-4ca1-9cb8-87c851a7ea25_grande.jpg?v=1483469893">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <strike>26.750 DT</strike> <span class="text-danger">20.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Viande fraiche
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: #159bb6;border-color: #159bb6;"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#noticeModal">
                                <div style="padding: 10px;">
                                        <span class="label label-danger shipping"
                                              style="font-size: 14px;border-radius: 5px;">-20% </span>
                                </div>
                                <img class="img"
                                     src="https://dutchcoffeepack.com/wp-content/uploads/2015/06/1-kg-zak-2-1024x668.jpg?x50557">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <strike>7500 DT</strike> <span class="text-danger">5.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Café bondin
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: #159bb6;border-color: #159bb6;"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#noticeModal">
                                <div style="padding: 10px;">
                                        <span class="label label-danger shipping"
                                              style="font-size: 14px;border-radius: 5px;">-15% </span>
                                </div>
                                <img class="img" src="assets/img/milk.jpg" style="max-height: 300px;">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <strike>1.750 DT</strike> <span class="text-danger">0.700 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Lait
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: #159bb6;border-color: #159bb6;"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#noticeModal">
                                <div style="padding: 10px;">
                                        <span class="label label-danger shipping"
                                              style="font-size: 14px;border-radius: 5px;">-15% </span>
                                </div>
                                <img class="img" src="/assets/img/wine.png">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <strike>26.750 DT</strike> <span class="text-danger">20.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Vin rouge
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon" style="color: white;">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: #159bb6;border-color: #159bb6;"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>