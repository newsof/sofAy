<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    <title>@yield('title')</title>
    @section('metatag')

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
        <meta name="viewport" content="width=device-width"/>

        <base href="{{ url('') }}">
    @show

    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.ico')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png')}}">


    @section('topstyle')
    <!--     Fonts and icons     -->
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    @show

    @section('top-costum-style')
    @show
    @section('topscript')
        <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-migrate.min.js') }}"></script>
    @show
</head>
<body>
<app-root></app-root>
<router-outlet></router-outlet>
<!-- Routed views go here -->
<div id="bodyy" style="margin-top: 170px;">

    @include('frontend.partials.header')


    @section('content')
    @show


</div>

@include('frontend.partials.footer')

@include('frontend.partials.sidenav')

@section('remainscript')

    <script src="{{ asset('/assets/js/jquery-ui-1.12.1.custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/tether.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

    <!-- Switches -->
    <script src="{{ asset('/assets/js/bootstrap-switch.min.js') }}"></script>

    <!--  Plugins for Slider -->
    <script src="{{ asset('/assets/js/nouislider.js') }}"></script>

    <!--  Photoswipe files -->
    <script src="{{ asset('/assets/js/photo_swipe/photoswipe.min.js') }}"></script>
    <script src="{{ asset('/assets/js/photo_swipe/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ asset('/assets/js/photo_swipe/init-gallery.js') }}"></script>

    <!--  Plugins for Select -->
    <script src="{{ asset('/assets/js/bootstrap-select.js') }}"></script>

    <!--  for fileupload -->
    <script src="{{ asset('/assets/js/jasny-bootstrap.min.js') }}"></script>

    <!--  Plugins for Tags -->
    <script src="{{ asset('/assets/js/bootstrap-tagsinput.js') }}"></script>

    <!--  Plugins for DateTimePicker -->
    <script src="{{ asset('/assets/js/moment.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('/assets/js/paper-kit.js?v=2.0.1') }}"></script>



@show
@section('customscript')

    <script src="{{ asset('/assets/js/index.js') }}"></script>
    <script type="text/javascript">
        function openNav() {
            document.getElementById("fix").style.width = ($(window).width() - 300) + "px";
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("bodyy").style.marginRight = "300px";


        }

        /* Set the width of the side navigation to 0 */
        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("bodyy").style.marginRight = "0";
            document.getElementById("fix").style.width = ($(window).width()) + "px";
        }
    </script>
    {{--<script src="{{ asset('assets/js/jsnow.js') }}"></script>--}}
    {{--<script !src="">--}}
        {{--/* <![CDATA[ */--}}
        {{--var snoweffect = {--}}
            {{--"show": "1",--}}
            {{--"flakes_num": "30",--}}
            {{--"falling_speed_min": "1",--}}
            {{--"falling_speed_max": "3",--}}
            {{--"flake_max_size": "20",--}}
            {{--"flake_min_size": "10",--}}
            {{--"vertical_size": "800",--}}
            {{--"flake_color": "#dbd4d4",--}}
            {{--"flake_zindex": "100000",--}}
            {{--"flake_type": "#10053",--}}
            {{--"fade_away": "1"--}}
        {{--};--}}
        {{--/* ]]> */--}}
    {{--</script>--}}
    {{--<script src="{{ asset('assets/js/wp-snow-effect-public.js') }}"></script>--}}
    {{--<!-- Styles -->--}}

@show
</body>
</html>