

<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header no-border-header">
                <h5 class="modal-title" id="myModalLabel"><strong><b> Panier</b> </strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="title">Shopping Cart Table</h4>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <div class="table">
                            <table class="table table-shopping">
                                <thead>
                                <tr>
                                    <th class="text-center"></th>
                                    <th></th>
                                    <th class="text-right">Price</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="img-container">
                                            <img src="{{ asset('assets/img/tables/agenda.png')}}" alt="Agenda">
                                        </div>
                                    </td>
                                    <td class="td-product">
                                        <strong>Get Shit Done Notebook</strong>
                                        <p>
                                            Most beautiful agenda for the office, really nice paper and black cover.
                                            Most beautiful agenda for the office.</p>
                                    </td>

                                    <td class="td-price">
                                        <small>€</small>
                                        49
                                    </td>
                                    <td class="td-number td-quantity">
                                        1
                                        <div class="btn-group">
                                            <button class="btn btn-sm btn-border btn-round"> -</button>
                                            <button class="btn btn-sm btn-border btn-round"> +</button>
                                        </div>
                                    </td>
                                    <td class="td-number">
                                        <small>€</small>
                                        49
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="img-container">
                                            <img src="{{ asset('assets/img/tables/stylus.jpg')}}" alt="Stylus">
                                        </div>
                                    </td>
                                    <td class="td-product">
                                        <strong>Stylus</strong>
                                        <p>Design is not just what it looks like and feels like. Design is how it
                                            works. </p>
                                    </td>

                                    <td class="td-price">
                                        <small>€</small>
                                        499
                                    </td>
                                    <td class="td-number td-quantity">
                                        2
                                        <div class="btn-group">
                                            <button class="btn btn-sm btn-border btn-round"> -</button>
                                            <button class="btn btn-sm btn-border btn-round"> +</button>
                                        </div>
                                    </td>
                                    <td class="td-number">
                                        <small>€</small>
                                        998
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="img-container">
                                            <img src="{{ asset('assets/img/tables/evernote.png')}}" alt="Evernote">
                                        </div>
                                    </td>
                                    <td class="td-product">
                                        <strong>Evernote iPad Stander</strong>
                                        <p>A groundbreaking Retina display. All-flash architecture.
                                            Fourth-generation Intel processors.</p>
                                    </td>

                                    <td class="td-price">
                                        <small>€</small>
                                        799
                                    </td>
                                    <td class="td-number td-quantity">
                                        1
                                        <div class="btn-group">
                                            <button class="btn btn-sm btn-border btn-round"> -</button>
                                            <button class="btn btn-sm btn-border btn-round"> +</button>
                                        </div>
                                    </td>
                                    <td class="td-number">
                                        <small>€</small>
                                        799
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td class="td-total">
                                        Total
                                    </td>
                                    <td class="td-total">
                                        <small>€</small>
                                        12,999
                                    </td>
                                    <!--
                                                                    <td> <button type="button" class="btn btn-info btn-l">Complete Purchase <i class="fa fa-chevron-right"></i></button></td>

                                                                    <td></td>
                                    -->
                                </tr>
                                <tr class="tr-actions">
                                    <td colspan="3"></td>
                                    <td colspan="2" class="text-right">
                                        <button type="button" class="btn btn-danger btn-lg">Complete Purchase <i
                                                    class="fa fa-chevron-right"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
