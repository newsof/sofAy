
<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header no-border-header">
                <h5 class="modal-title" id="myModalLabel"><strong><b> COCA COLA</b> </strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <img src="/assets/img/coca.png" itemprop="thumbnail" alt="Image description"
                     class="small-horizontal-image img-rounded img-responsive">
                <div class="card-description">
                    Lorem ipsum dolor sit amet, vim no tota liber, est vivendum splendide at. Wisi delicata ne his.
                    Ad eleifend splendide neglegentur nam, at iusto homero pro, nec nominavi eloquentiam et. An usu
                    justo ignota fuisset. Eum eu soleat voluptatum comprehensam, ad movet utinam nostrum eum.
                </div>
                <br>
                <div class="price">
                    <span class="text-danger">8.000 DT</span>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            <div class="">
                                <button class="btn btn-default btn-just-icon" style="color: white;">-</button>
                                1
                                <button class="btn btn-default btn-just-icon" style="color: white;">+</button>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn" style="background: #159bb6;border-color: #159bb6;">Ajouter
                            au panier</i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
