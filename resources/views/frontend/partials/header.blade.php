<div class="topheader" id="fix" >
    <nav class="navbar navbar-toggleable-md bg-default" style="border-width: 0px;box-shadow: none;">
        <div class="container" style="">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>
            <a class="navbar-brand primary" href="/home" style="color: #159bb6;"><img
                        src="{{ asset('/assets/img/jibly.png') }}" height="40"></a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <form class="form-inline" style="margin-left: 30px;">
                        <div class="input-group" style="width: 400px;">
                            <span class="input-group-addon"><i class="nc-icon nc-zoom-split" aria-hidden="true"></i></span>
                            <input type="text" placeholder="Recherche" class="form-control">
                        </div>
                    </form>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a href="#paper-kit" class="nav-link" data-toggle="dropdown" width="30" height="30">
                            <div class="profile-photo-small">
                                <img src="{{ asset('/assets/img/faces/joe-gardner-2.jpg') }}" alt="Circle Image"
                                     class="img-circle img-responsive img-no-padding">
                            </div>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-info">
                            <li class="dropdown-item"><a href="#paper-kit">Me</a></li>
                            <li class="dropdown-item"><a href="#paper-kit">Settings</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-item"><a href="#paper-kit">Sign out</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="btn btn-just-icon"
                                style="background: #159bb6;border-color: #159bb6;" onclick="openNav()"><i
                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                    </li>
                </ul>
            </div>

        </div>


    </nav>

    <div class="container" id="catmenu">
        <div class="col-md-12" style="padding: 0;margin: 0;">
            <div class="row__inner" style="overflow-x: auto;overflow-y: hidden;margin-top: 0;">



                @foreach($allcat as $cattem)

                <div class="tile cat{{$cattem['id']}} {{$cattem['slug']}} " >
                    {{----}}
                    <div class="tile__media icon" style="@if($cat==1) {{ 'animation-delay: -0.75s; animation-duration: .25s;' }} @endif">
                        <div class="card tile__img" >
                            <div class="card-block author" style="display:table-cell; vertical-align:middle; text-align:center;width: 100%;height: 100%;">
                                <img src="{{ asset('assets/img/icons/'.$cattem['slug'].'.png') }}" alt="..." class="avatar img-raised">
                                <a routerLink="'alimentation/{{ $cattem['slug'] }}'" >
                                    <strong><b>{{$cattem['label']}}</b></strong>
                                </a>


                            </div>
                        </div>
                    </div>
                </div>

                @endforeach
            </div>

        </div>
        <div class="title-category-fix container hide"  id="title-category-fix" >
            <strong><b>Boisson</b></strong> <!----><a class="btn btn-info btn-sm viewplus" href="" style="color: white;float: right;">Voir plus »<i class="iconf-right"></i></a>
        </div>
    </div>

</div>
