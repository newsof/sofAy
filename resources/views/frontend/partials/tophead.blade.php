<div class="topheader" id="fix" >


    <div class="container" id="catmenu">
        <div class="col-md-12" style="padding: 0;margin: 0;">
            <div class="row__inner" style="overflow-x: auto;overflow-y: hidden;margin-top: 0;">



                @foreach($allcat as $cattem)

                <div class="tile cat{{$cattem['id']}} {{$cattem['slug']}} " >
                    {{----}}
                    <div class="tile__media icon" style="@if($cat==1) {{ 'animation-delay: -0.75s; animation-duration: .25s;' }} @endif">
                        <div class="card tile__img" >
                            <div class="card-block author" style="display:table-cell; vertical-align:middle; text-align:center;width: 100%;height: 100%;">
                                <img src="{{ asset('/assets/img/icons/'.$cattem['slug'].'.png') }}" alt="..." class="avatar img-raised">
                                <a href="{{ url('/alimentation/'.$cattem['slug'])}}" >
                                    <strong><b>{{$cattem['label']}}</b></strong>
                                </a>


                            </div>
                        </div>
                    </div>
                </div>

                @endforeach
            </div>

        </div>
        <div class="title-category-fix container hide"  id="title-category-fix" >
            <strong><b>Boisson</b></strong> <!----><a class="btn btn-info btn-sm viewplus" href="" style="color: white;float: right;">Voir plus »<i class="iconf-right"></i></a>
        </div>
    </div>

</div>
