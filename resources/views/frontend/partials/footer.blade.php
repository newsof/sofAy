<footer class="footer section-dark">
    <div class="container">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li><a href="http://www.creative-tim.com"><img src="{{ asset('assets/img/jibly.png') }}" height="30"></a></li>

                </ul>
            </nav>
            <div class="credits ml-auto">
                  <span class="copyright">
                     © <script>document.write(new Date().getFullYear())</script>, Powred <i class="fa fa-heart heart"></i> by Monresto
                  </span>
            </div>
        </div>
    </div>
</footer>
