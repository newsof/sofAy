<div id="mySidenav" class="sidenav">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: 30px;"
            onclick="closeNav()">&times;
    </button>
    <div class="container" style="margin-top: 50px;">
        <div class="panier-side">
            <h4><strong><b>Mon panier</b></strong></h4>
            <hr>
            <div class="row itemincart" style="font-size: 12px;">

                <div class="col-md-12 item-title">
                    Coca cola
                </div>
                <div class="col-md-3 item-img">
                    <a href="/products/sega-soft-sports-shoes?variant=46074334214" title="" class="product-image">
                        <img src="https://cdn.shopify.com/s/files/1/2180/5147/products/baby37_small.jpg" alt=" ">
                    </a>
                </div>
                <div class="col-md-6 item-unitprice">
                    2 * 16.000 <span>DT</span>
                </div>

                <div class="col-md-3 item-plus-min">

                    <div class="btn-group">
                        <button class="btn btn-sm btn-border"> -</button>
                        <button class="btn btn-sm btn-border"> +</button>
                    </div>
                </div>

                <div class="closebt">

                    <button type="button" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Remove" class="btn btn-danger btn-link btn-sm">
                        <i class="fa fa-times"></i>
                    </button>
                </div>

            </div>
            <hr>
            <div class="row itemincart" style="font-size: 12px;">

                <div class="col-md-12 item-title">
                    Coca cola
                </div>
                <div class="col-md-3 item-img">
                    <a href="/products/sega-soft-sports-shoes?variant=46074334214" title="" class="product-image">
                        <img src="https://cdn.shopify.com/s/files/1/2180/5147/products/baby37_small.jpg" alt=" ">
                    </a>
                </div>
                <div class="col-md-6 item-unitprice">
                    2 * 16.000 <span>DT</span>
                </div>

                <div class="col-md-3 item-plus-min">

                    <div class="btn-group">
                        <button class="btn btn-sm btn-border"> -</button>
                        <button class="btn btn-sm btn-border"> +</button>
                    </div>
                </div>

                <div class="closebt">

                    <button type="button" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Remove" class="btn btn-danger btn-link btn-sm">
                        <i class="fa fa-times"></i>
                    </button>
                </div>

            </div>

            <hr>
            <div class="row itemincart" style="font-size: 12px;">

                <div class="col-md-12 item-title">
                    Coca cola
                </div>
                <div class="col-md-3 item-img">
                    <a href="/products/sega-soft-sports-shoes?variant=46074334214" title="" class="product-image">
                        <img src="https://cdn.shopify.com/s/files/1/2180/5147/products/baby37_small.jpg" alt=" ">
                    </a>
                </div>
                <div class="col-md-6 item-unitprice">
                    2 * 16.000 <span>DT</span>
                </div>

                <div class="col-md-3 item-plus-min">

                    <div class="btn-group">
                        <button class="btn btn-sm btn-border"> -</button>
                        <button class="btn btn-sm btn-border"> +</button>
                    </div>
                </div>

                <div class="closebt">

                    <button type="button" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Remove" class="btn btn-danger btn-link btn-sm">
                        <i class="fa fa-times"></i>
                    </button>
                </div>

            </div>

            <hr>
            <div class="row itemincart" style="font-size: 12px;">

                <div class="col-md-12 item-title">
                    Coca cola
                </div>
                <div class="col-md-3 item-img">
                    <a href="/products/sega-soft-sports-shoes?variant=46074334214" title="" class="product-image">
                        <img src="https://cdn.shopify.com/s/files/1/2180/5147/products/baby37_small.jpg" alt=" ">
                    </a>
                </div>
                <div class="col-md-6 item-unitprice">
                    2 * 16.000 <span>DT</span>
                </div>

                <div class="col-md-3 item-plus-min">

                    <div class="btn-group">
                        <button class="btn btn-sm btn-border"> -</button>
                        <button class="btn btn-sm btn-border"> +</button>
                    </div>
                </div>

                <div class="closebt">

                    <button type="button" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Remove" class="btn btn-danger btn-link btn-sm">
                        <i class="fa fa-times"></i>
                    </button>
                </div>

            </div>


            <hr>

            <div class="row">

                <div class="col-md-12">
                    </br> <strong><b>Total : 50.000 DT</b></strong> </br>
                </div><hr>

                <div class="col-md-12">
                    <button type="button" class="btn btn-danger btn-lg">Confirmer le panier <i
                                class="fa fa-chevron-right"></i></button>
                </div>

            </div>


        </div>
    </div>


</div>