@extends('frontend.layouts.app')

@section('title', 'Jibly')

<base href="/">
@section('metatag')
    @parent
@endsection
@section('topstyle')
    @parent

    <link href="{{ asset('/assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{ asset('/assets/css/nucleo-icons.css')}}" rel="stylesheet">
    <link href="{{ asset('/assets/css/paper-kit.css?v=2.0.1')}}" rel="stylesheet"/>
@endsection

@section('top-costum-style')
    <link href="{{ asset('/assets/css/demo.css')}}" rel="stylesheet"/>
    <link href="{{ asset('/assets/css/style.css')}}" rel="stylesheet">
@endsection
@section('topscript')
    @parent
@endsection

@section('content')
    @parent
    <div class="homeContent">

        @include('frontend.sections.promotions')
        @include('frontend.sections.boissons')
    </div>
    @include('frontend.modals.productinfo')
@endsection

@section('remainscript')
    @parent
@endsection
@section('customscript')
    @parent
    <script>

        var stickyHeaders = (function () {

            var $window = $(window),
                $stickies;

            var load = function (stickies) {

                if (typeof stickies === "object" && stickies instanceof jQuery && stickies.length > 0) {

                    $stickies = stickies.each(function () {

                        var $thisSticky = $(this).wrap('<div class="followWrap" />');

                        $thisSticky
                            .data('originalPosition', $thisSticky.offset().top)
                            .data('originalHeight', $thisSticky.outerHeight())
                            .parent()
                            .height($thisSticky.outerHeight());
                    });

                    $window.off("scroll.stickies").on("scroll.stickies", function () {
                        _whenScrolling();
                    });
                }
            };

            var _whenScrolling = function () {

                $stickies.each(function (i) {

                    var $thisSticky = $(this),
                        $stickyPosition = $thisSticky.data('originalPosition');

                    if ($stickyPosition <= $window.scrollTop()) {

                        var $nextSticky = $stickies.eq(i + 1),
                            $nextStickyPosition = $nextSticky.data('originalPosition') - $thisSticky.data('originalHeight');

                        $thisSticky.addClass("fixed");

                        if ($nextSticky.length > 0 && $thisSticky.offset().top >= $nextStickyPosition) {

                            $thisSticky.addClass("absolute").css("top", $nextStickyPosition);
                        }

                    } else {

                        var $prevSticky = $stickies.eq(i - 1);

                        $thisSticky.removeClass("fixed");

                        if ($prevSticky.length > 0 && $window.scrollTop() <= $thisSticky.data('originalPosition') - $thisSticky.data('originalHeight')) {

                            $prevSticky.removeClass("absolute").removeAttr("style");
                        }
                    }
                });
            };

            return {
                load: load
            };
        })();

        $(function () {
            stickyHeaders.load($(".followMeBar"));
        });

    </script>


@endsection
<!--<div id="app"> </div>
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
<script src="{{asset('js/app.js')}}"></script>-->

<script type="text/javascript" src="/dist/inline.bundle.js"></script>
<script type="text/javascript" src="/dist/polyfills.bundle.js"></script>
{{--<script type="text/javascript" src="dist/styles.bundle.js"></script>--}}
{{--<script type="text/javascript" src="dist/vendor.bundle.js"></script>--}}
<script type="text/javascript" src="/dist/main.bundle.js"></script>