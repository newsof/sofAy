<section class="container" id="productCont">
  
    <div class="products-list" data-color="{{ $catinfo->description }}" >
        <div class="container">

            <div class="row" style="margin: 0;">
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/water.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">5.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Eau
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color:{{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/coca.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">12.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Coca cola
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img"
                                     src="https://images.firstwefeast.com/complex/image/upload/pqsokipggn3dtkbl1hgb">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">8.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Coca cola
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/wine.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">24.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Vin rouge
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/water.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">5.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Eau
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/coca.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">12.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Coca cola
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img"
                                     src="https://images.firstwefeast.com/complex/image/upload/pqsokipggn3dtkbl1hgb">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">8.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Coca cola
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/wine.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">24.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Vin rouge
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/water.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">5.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Eau
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/coca.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">12.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Coca cola
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img"
                                     src="https://images.firstwefeast.com/complex/image/upload/pqsokipggn3dtkbl1hgb">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">8.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Coca cola
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-blog">
                        <div class="card-image">
                            <a href="#pablo" data-toggle="modal" data-target="#cartModal">
                                <img class="img" src="{{ asset('assets/img/wine.png') }}">
                                <div style="padding: 5px;">
                                    <div class="price">
                                        <span class="text-danger">24.000 DT</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-block text-center">
                            <h6 class="card-title">
                                Vin rouge
                            </h6>
                            <div class="card-description">
                                Cards are an interaction model that are spreading...
                            </div>
                            <div class="card-footer" style="width: 100%;">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12" style="padding: 0px;">
                                        <div>
                                            <div class="">
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    -
                                                </button>
                                                1
                                                <button class="btn btn-default btn-just-icon"
                                                        style="color: white;background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};">
                                                    +
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding: 0px;">
                                        <button type="button" class="btn btn-just-icon"
                                                style="background: {{ $catinfo->description }};border-color: {{ $catinfo->description }};"><i
                                                    class="nc-icon nc-basket danger" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>
</section>