@extends('frontend.layouts.app')

@section('title', 'Jibly')

@section('metatag')
    @parent
    <meta name="description" content=" "/>
@endsection
@section('topstyle')
    @parent

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('assets/css/nucleo-icons.css') }}">
    <link href="{{ asset('assets/css/paper-kit.css?v=2.0.1') }}" rel="stylesheet"/>
@endsection

@section('top-costum-style')
    <link href="{{ asset('assets/css/demo.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endsection
@section('topscript')
  @parent
@endsection

@section('content')

    @include('frontend.products.bycat')
    @include('frontend.modals.productinfo')
    @include('frontend.modals.panier')
@endsection

@section('remainscript')
    @parent
@endsection
@section('customscript')
    @parent

    <script !src="">
        $(function(){
            var $bodyy = $('#bodyy');
            var catmenuwidth=$("#catmenu").width();
            $("#title-category-fix").width(catmenuwidth-20).show();
            $bodyy.css('padding-top',(parseInt($bodyy.css('padding-top'))+50)+'px');
        })

    </script>


@endsection