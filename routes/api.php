<?php
use App\Http\Resources\ProductCategoryResource;
use App\Models\ProductCategory;
$api = app('Dingo\Api\Routing\Router');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
// Include CORS -> to allow access headers

Route::group(['middleware' => 'cors'],

function () {
		Route::get('title', function () {
				return response()->json([
						'data'     => [
							'title'   => 'Page Title',
							'message' => 'Example to show how to use service'
						]
					]);
			});

		Route::namespace ('Api')->group(function () {
				Route::get('/category', 'ApiController@getCategorys');

				Route::get('/allcategorys', function () {
						return new ProductCategoryResource(ProductCategory::all());
					});

			});

	});

$api->version('v1', ['namespace' => 'App\Http\Controllers\Backend'], function ($api) {

		$api->group(['prefix' => 'commande'], function ($api) {
				$api->get('/', 'CommandeController@listCommandes');
			});

	});
