# Laravel 5.5 + Angular 5 + AdminLTE

  

This is a simple application starter for [Laravel 5.5.x](https://laravel.com) + [Angular 5](https://angular.io/) and [AdminLTE](https://github.com/almasaeed2010/AdminLTE).
 

## Installation

- Clone the repository
- then run `composer install & npm install`

### Development

Run `npm run start` to start a server that'll run on `http://localhost:8000` or `npm run build-dev` to just compile the assets to public/build

### Build

You can also run `npm run build` to use webpack.config.js configuration instead of Laravel Mix.

### Laravel Mix

You can use Laravel Mix by running `npm run dev` or `npm run production`, The output will be published to public/build but it'll be incomplete and won't work with mix() functions in the layout by default.
 
### License
This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
