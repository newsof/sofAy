<?php

namespace Deployer;

require 'recipe/laravel-deployer.php';

/*
|--------------------------------------------------------------------------
| Define your options
|--------------------------------------------------------------------------
|
| Here, you can customize which files and folders should be shared between
| releases, change your repository URL, the name of your application,
| enable statistical data collection from Deployer, disable the
| use of a tty when using git and much more.
|
*/

set('application', 'Jibly');
set('repository', 'https://github.com/aminodrago/testLaravel5Angular5');
set('allow_anonymous_stats', false);
set('git_tty', true); 

add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);

/*
|--------------------------------------------------------------------------
| Configure your hosts
|--------------------------------------------------------------------------
|
| This is what Deployer will use to connect to your servers. You can set up
| multiple hosts including localhosts to enable deployment directly from 
| the server (useful if you are using Laravel Forge for example).
|
*/

host('minocodi.com')
    ->set('deploy_path', '/var/www/html');

/*
|--------------------------------------------------------------------------
| Write your own tasks
|--------------------------------------------------------------------------
|
| A handful of helpful tasks are already available for you to use
| (check out `php artisan deploy:list`) but here you can define 
| your own ones to automate any kind of custom deployment logic.
|
*/

desc('Example task');
task('jibly:helloworld', 'echo "hello world!"');

/*
|--------------------------------------------------------------------------
| Add more to your deployments with hooks
|--------------------------------------------------------------------------
|
| Hooks enable you to attach new tasks before or after the execution of
| other tasks. Run `php artisan deploy:dump deploy` to checkout where to
| hook on the current deployment flow. For a complete redefinition of 
| the deployment process, you can directly override the `deploy` task.
|
*/

// Npm 
after('deploy:update_code', 'npm:install');
after('npm:install', 'npm:production');