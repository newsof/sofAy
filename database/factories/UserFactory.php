<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;

    return [
        'login' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'tel' => $faker->phoneNumber,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'creation_date' =>$faker->dateTime($max = 'now', $timezone = null) ,
        'update_date' => $faker->dateTime($max = 'now', $timezone = null) ,
    ];
});
