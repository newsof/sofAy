<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Transformers\CommondeTransformer;

// Transformers
use App\Models\Commande;

// Validators

// Models
use Illuminate\Http\Request;

class CommandeController extends Controller {
	public function listCommandes(Request $request) {

		/*
		the commandes list has extra big data
		if we will get all data inside we risk to access the memory limit
		so we decided to work with limit and current curser
		 */
		$total         = Commande::count();
		$limit         = $request->input('limit', 10);
		$currentCursor = $request->input('cursor', null);
		$resources     = Commande::take($limit);
		if ($currentCursor) {
			$resources = $resources->skip($currentCursor);
		}
		$resources = $resources->get();

		// Get includes ( the relations that frontEnd want to display with the order entity)
		$includes = explode(',', $request->input('include'));
		//to return a transformed result and parse what relation you want to incude in the result and the field to display wthin
		return $this->collection($resources, new CommondeTransformer, $includes, [], null, $total);
	}

}
