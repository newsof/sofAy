<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory as ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;


class  MarketPlaceController extends Controller
{


    private $categorys;

    private $allcat=array();
    private $products=array();
    /**
     * HomeController constructor.
     */
    public function __construct(ProductCategory $categorys)
    {
       $this->categorys=$categorys;

        $this->allcat=$this->categorys->getAllByPartner(246);;
    }


    public function index($sid)
    {
         $cat=2;
         $this->allcat=$this->categorys->getAllByPartner(246);;
         $catinfo=array();
         $allcat=$this->allcat;

	    return view('frontend.home.ng',  compact('cat', 'allcat'  ));
    }
    public function showinfos()
    {
         $cat=2;
         $this->allcat=$this->categorys->getAllByPartner(246);;
         $catinfo=array();
         $allcat=$this->allcat;
	    return view('frontend.home.ng',  compact('cat', 'allcat'  ));

    }



}
