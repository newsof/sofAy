<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory as ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;


class CategoryByMarketPlaceController extends Controller
{


    private $categorys;

    private $allcat=array();
    private $products=array();
    /**
     * HomeController constructor.
     */
    public function __construct(ProductCategory $categorys)
    {
       $this->categorys=$categorys;

        $this->allcat=$this->categorys->getAllByPartner(246);
    }


    public function index($MSlug)
    {
         $cat=2;
         $this->allcat=$this->categorys->getAllByPartner(246);
         $catinfo=array();
         $allcat=$this->allcat;

        return view('frontend.home.index',  compact('cat', 'allcat'  ));
    }

    public function showCategory($sid,$cid)
    {
         $cat=$cid;
         $catinfo=$this->categorys->find($cid);
         $this->allsubcat=$this->categorys->getAllByCat($sid);
         $allcat=$this->allcat;
         $products=$this->products;

       // return view('frontend.category.list');
	    return view('frontend.home.ng',  compact('cat', 'allcat','allsubcat','products','catinfo'));
    }

}
