<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory as ProductCategory;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    /**
     * @var ProductCategory
     */
    private $categorys;
    /**
     * @var array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    private $allcat = array();

    /**
     * HomeController constructor.
     */
    public function __construct(ProductCategory $categorys)
    {

        $this->categorys = $categorys;
        $this->allcat = $this->categorys->getAllByPartner(246);
    }

    public function getCategorys()
    {

        $cat = 1;
        $allcat = $this->allcat;

        return $allcat;

    }

}
