<?php

namespace App\Http\Transformers;

use App\Models\Zone;

use League\Fractal\TransformerAbstract;

class ZoneTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
	];

	public function transform(Zone $Zone) {
		$tmp = [
			'id'        => $Zone->id,
			'cp'        => $Zone->cp,
			'slug'      => $Zone->slug,
			'latitude'  => $Zone->latitude,
			'longitude' => $Zone->longitude,
			'is_active' => $Zone->is_active,
			'date'      => $Zone->date,
			'ville'     => $Zone->Ville

		];
		return $tmp;
	}

}