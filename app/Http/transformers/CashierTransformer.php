<?php

namespace App\Http\Transformers;

use App\Models\Caissier;

use League\Fractal\TransformerAbstract;

class CashierTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(Caissier $Caissier) {
		$tmp = [
			'id' => $Caissier->id
		];
		return $tmp;
	}

	/*
public function includeClient($resource) {
return $resource->client?$this->item($resource->client, new ClientTransformer($this->client)):$resource->client;
return null;

}
 */

}