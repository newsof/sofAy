<?php

namespace App\Http\Transformers;

use App\Models\PaymentModes;

use League\Fractal\TransformerAbstract;

class PaymentModeTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(PaymentModes $PaymentMode) {
		$tmp = [
			'id'            => $PaymentMode->id,
			'title'         => $PaymentMode->title,
			'description'   => $PaymentMode->description,
			'image'         => $PaymentMode->image,
			'payment_image' => $PaymentMode->payment_image,
			'display_order' => $PaymentMode->display_order,

		];
		return $tmp;
	}

}