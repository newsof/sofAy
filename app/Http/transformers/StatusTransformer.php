<?php

namespace App\Http\Transformers;

use App\Models\Status;

use League\Fractal\TransformerAbstract;

class StatusTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(Status $Status) {
		$tmp = [
			'id'          => $Status->id,
			'code'        => $Status->code,
			'title'       => $Status->title,
			'description' => $Status->description

		];
		return $tmp;
	}

	/*
public function includeClient($resource) {
return $resource->client?$this->item($resource->client, new ClientTransformer($this->client)):$resource->client;
return null;

}
 */

}