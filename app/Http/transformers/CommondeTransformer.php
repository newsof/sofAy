<?php

namespace App\Http\Transformers;

use App\Models\Client;
use App\Models\Commande;

use League\Fractal\TransformerAbstract;

class CommondeTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	/*to parse includes , we must define them in $availableIncludes array and laravel will call the method 	that also must be defined bellow with name 'include[the element of the $availableIncludes Array ]'
	for example in client element we must create a method called includeClient to let laravel call it without any error
	 */
	protected $availableIncludes =
	[
		'client',
		'adressA',
		'adressB',
		'partenaire',
		'livreur',
		'referral',
		'cashier',
		'status',
		'paymentMode'
	];

	public function transform(Commande $Commande) {
		/*
		'id_client',
		'id_address',
		'id_partenaire',
		'id_livreur1',
		'id_referral',
		'id_language
		''id_cashier' 'id_promo_campaign' 'id_option'
		 */
		$tmp = [
			'id'                        => $Commande->id,
			'code'                      => $Commande->code,
			'order_type'                => $Commande->order_type,
			'date_envoie'               => $Commande->date_envoie,
			'heure_envoie'              => $Commande->heure_envoie,
			'pour_le'                   => $Commande->pour_le,
			'code_promo'                => $Commande->code_promo,
			'etat'                      => $Commande->etat,
			'tarif'                     => $Commande->tarif,
			'sous_total'                => $Commande->sous_total,
			'tva'                       => $Commande->tva,
			'balance_consumed'          => $Commande->balance_consumed,
			'balance'                   => $Commande->balance,
			'etat_paiement'             => $Commande->etat_paiement,
			'mode_paiement'             => $Commande->mode_paiement,
			'etat_pay_resto'            => $Commande->etat_pay_resto,
			'pay_resto_date'            => $Commande->pay_resto_date,
			'etat_resto'                => $Commande->etat_resto,
			'date_heure_refus'          => $Commande->date_heure_refus,
			'refuse_reason'             => $Commande->refuse_reason,
			'conf'                      => $Commande->conf,
			'conf_print'                => $Commande->conf_print,
			'agent'                     => $Commande->agent,
			'android'                   => $Commande->android,
			'type'                      => $Commande->type,
			'statut_mail_notation'      => $Commande->statut_mail_notation,
			'mode_contact_client'       => $Commande->mode_contact_client,
			'application_type'          => $Commande->application_type,
			'is_awaiting'               => $Commande->is_awaiting,
			'montant_verser'            => $Commande->montant_verser,
			'token'                     => $Commande->token,
			'etat_process'              => $Commande->etat_process,
			'etat_response'             => $Commande->etat_response,
			'review_status'             => $Commande->review_status,
			'review_answer_date'        => $Commande->review_answer_date,
			'supp_frais'                => $Commande->supp_frais,
			'new_version'               => $Commande->new_version,
			'signal_cmd'                => $Commande->signal_cmd,
			'description'               => $Commande->description,
			'payment_delivery'          => $Commande->payment_delivery,
			'confirm_payment_delivery'  => $Commande->confirm_payment_delivery,
			'remain_topay'              => $Commande->remain_topay,
			'Is_order_paid_cashier'     => $Commande->Is_order_paid_cashier,
			'is_order_paid_deliveryman' => $Commande->is_order_paid_deliveryman,
			'transfer_mobicach'         => $Commande->transfer_mobicach,
			'id_language'               => $Commande->id_language,
			'orderType'                 => $Commande->orderType,
			'orderOrigine'              => $Commande->orderOrigine,
			'delivery_time'             => $Commande->delivery_time,
			'orderOptions'              => $Commande->orderOptions,

		];
		return $tmp;
	}
	public function includeClient($resource) {
		return $resource->client?$this->item($resource->client, new ClientTransformer($this->params)):$resource->client;
	}

	public function includeAdressA($resource) {
		return $resource->adressA?$this->item($resource->adressA, new AdressTransformer($this->params)):$resource->adressA;
	}

	public function includeAdressB($resource) {
		return $resource->adressB?$this->item($resource->adressB, new AdressTransformer($this->params)):$resource->adressB;
	}

	public function includePartenaire($resource) {
		return $resource->partenaire?$this->item($resource->partenaire, new PartenaireTransformer($this->params)):$resource->partenaire;
	}

	public function includeLivreur($resource) {
		return $resource->livreur?$this->item($resource->livreur, new LivreurTransformer($this->params)):$resource->livreur;
	}

	public function includeReferral($resource) {
		return $resource->referral?$this->item($resource->referral, new ReferralTransformer($this->params)):$resource->referral;
	}

	public function includeCashier($resource) {
		return $resource->cashier?$this->item($resource->cashier, new CashierTransformer($this->params)):$resource->cashier;
	}

	public function includeStatus($resource) {
		return $resource->status?$this->item($resource->status, new StatusTransformer($this->params)):$resource->status;
	}

	public function includePaymentMode($resource) {
		return $resource->PaymentMode?$this->item($resource->PaymentMode, new PaymentModeTransformer($this->params)):$resource->PaymentMode;
	}

}