<?php

namespace App\Http\Transformers;

use App\Models\Referral;

use League\Fractal\TransformerAbstract;

class ReferralTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(Referral $Referral) {
		$tmp = [
			'id' => $Referral->id
		];
		return $tmp;
	}

	/*
public function includeClient($resource) {
return $resource->client?$this->item($resource->client, new ClientTransformer($this->client)):$resource->client;
return null;

}
 */

}