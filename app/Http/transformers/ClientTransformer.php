<?php

namespace App\Http\Transformers;

use App\Models\Client;

use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(Client $Client) {
		$tmp = [
			'id'     => $Client->id,
			'nom'    => $Client->nom,
			'prenom' => $Client->prenom
		];
		return $tmp;
	}

	/*
public function includeClient($resource) {
return $resource->client?$this->item($resource->client, new ClientTransformer($this->client)):$resource->client;
return null;

}
 */

}