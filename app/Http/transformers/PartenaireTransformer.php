<?php

namespace App\Http\Transformers;

use App\Models\Partenaire;

use League\Fractal\TransformerAbstract;

class PartenaireTransformer extends TransformerAbstract {
	private $params = [
	];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[

		'zone'
	];

	public function transform(Partenaire $Partenaire) {
		$tmp = [
			'id'                  => $Partenaire->id,
			'deliverySector'      => $Partenaire->deliverySector,
			'type'                => $Partenaire->Type,
			'category'            => $Partenaire->Category,
			'status'              => $Partenaire->Status,
			'ville'               => $Partenaire->Ville,
			'Commissions'         => $Partenaire->Commissions,
			'Discounts'           => $Partenaire->Discounts,
			'PartnerFamilies'     => $Partenaire->PartnerFamilies,
			'PartnerHoliday'      => $Partenaire->PartnerHoliday,
			'PartnerImages'       => $Partenaire->PartnerImages,
			'PartnerPaymentModes' => $Partenaire->PartnerPaymentModes,
			'ProductCategory'     => $Partenaire->ProductCategory,
			'Sectors'             => $Partenaire->Sectors,
			'Affiliate'           => $Partenaire->Affiliate,
			'PartnerTokens'       => $Partenaire->PartnerTokens,
			'WorkingDays'         => $Partenaire->WorkingDays,
			'Zones'               => $Partenaire->Zones,
			'Zonning'             => $Partenaire->Zonning,

		];
		return $tmp;
	}

	public function includeZone($resource) {
		return $resource->zone?$this->item($resource->zone, new ZoneTransformer($this->params)):$resource->zone;
	}

}