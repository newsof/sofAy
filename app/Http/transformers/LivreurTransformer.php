<?php

namespace App\Http\Transformers;

use App\Models\Livreur;
use League\Fractal\TransformerAbstract;

class LivreurTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(Livreur $Livreur) {
		$tmp = [
			'id' => $Livreur->id
		];
		return $tmp;
	}

	/*
public function includeClient($resource) {
return $resource->client?$this->item($resource->client, new ClientTransformer($this->client)):$resource->client;
return null;

}
 */

}