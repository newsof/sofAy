<?php

namespace App\Http\Transformers;

use App\Models\Adresse;

use League\Fractal\TransformerAbstract;

class AdressTransformer extends TransformerAbstract {
	private $params = [];

	function __construct($params = []) {
		$this->params = $params;
	}

	protected $availableIncludes =
	[
		//	'client'
	];

	public function transform(Adresse $Address) {
		$tmp = [
			'id' => $Address->id
		];
		return $tmp;
	}

	/*
public function includeClient($resource) {
return $resource->client?$this->item($resource->client, new ClientTransformer($this->client)):$resource->client;
return null;

}
 */

}