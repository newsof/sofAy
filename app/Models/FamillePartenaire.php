<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_famille
 * @property int $id_partenaire
 * @property Famille $famille
 * @property Partenaire $partenaire
 */
class FamillePartenaire extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'famille_partenaire';

    /**
     * @var array
     */
    protected $fillable = ['id_famille', 'id_partenaire'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function famille()
    {
        return $this->belongsTo('App\Famille', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partenaire');
    }
}
