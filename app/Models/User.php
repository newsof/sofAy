<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


/**
 * @property int $id
 * @property int $id_partenaire
 * @property string $login
 * @property string $password
 * @property string $remember_token
 * @property string $email
 * @property string $tel
 * @property string $autorisation
 * @property string $type_acces
 * @property string $privilege
 * @property boolean $is_connected
 * @property string $last_connection_date
 * @property string $creation_date
 * @property string $update_date
 * @property Partenaire $partenaire
 * @property Jobtimer[] $jobtimers
 * @property Livreur[] $livreurs
 */
class User extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'utilisateurs';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['login', 'password', 'remember_token', 'email', 'tel', 'creation_date', 'update_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partenaire');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobtimers()
    {
        return $this->hasMany('App\Jobtimer', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function livreurs()
    {
        return $this->hasMany('App\Livreur', 'id_superviseur');
    }



}
