<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partner
 * @property int $id_article_family
 * @property int $id_article
 * @property int $id_company
 * @property int $pourcentage_remise
 * @property string $description
 * @property string $date_debut
 * @property string $date_fin
 * @property string $activation
 * @property string $type
 * @property boolean $is_forall
 * @property int $type_promotion
 * @property int $type_service
 * @property Partenaire $partenaire
 * @property FamillePlat $famillePlat
 * @property Plat $plat
 */
class PRemise extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'p_remise';

    /**
     * @var array
     */
    protected $fillable = ['id_partner', 'id_article_family', 'id_article', 'id_company', 'pourcentage_remise', 'description', 'date_debut', 'date_fin', 'activation', 'type', 'is_forall', 'type_promotion', 'type_service'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function famillePlat()
    {
        return $this->belongsTo('App\FamillePlat', 'id_article_family');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plat()
    {
        return $this->belongsTo('App\Plat', 'id_article');
    }
}
