<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerCommissions extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_commissions';
	protected $dates      = [];

	public function Partners() {
		return $this->belongsTo('App\Models\Partenaire', 'id_partner');
	}

}
