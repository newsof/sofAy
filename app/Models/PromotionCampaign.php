<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partner
 * @property string $code
 * @property string $amount
 * @property string $begin_date
 * @property string $end_date
 * @property boolean $is_multiple
 * @property boolean $is_active
 * @property Partenaire $partenaire
 */

class PromotionCampaign extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'promotion_campaign';
	protected $dates      = [];
	/**
	 * @var array
	 */
	protected $fillable = ['id_partner', 'code', 'amount', 'begin_date', 'end_date', 'is_multiple', 'is_active'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function partenaire() {
		return $this->belongsTo('App\Partenaire', 'id_partner');
	}
}
