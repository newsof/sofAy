<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkingDay extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'workingdays';
	protected $dates      = [];

}
