<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nom
 * @property string $statut
 * @property string $slug
 * @property Adress[] $adresses
 */

class Ville extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'cities';
	protected $dates      = [];

	/**
	 * @var array
	 */
	protected $fillable = ['nom', 'statut', 'slug'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function adresses() {
		return $this->hasMany('App\Adress', 'id_city');
	}
}
