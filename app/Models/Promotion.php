<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partner
 * @property int $id_client
 * @property string $sujet
 * @property string $notes
 * @property string $vpromo
 * @property string $date
 * @property int $vu
 * @property string $code
 * @property string $min_date
 * @property string $max_date
 * @property string $statut
 * @property Partenaire $partenaire
 * @property Client $client
 */
class Promotion extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_partner', 'id_client', 'sujet', 'notes', 'vpromo', 'date', 'vu', 'code', 'min_date', 'max_date', 'statut'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'id_client');
    }
}
