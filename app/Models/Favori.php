<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partenaire
 * @property int $id_plat
 * @property int $id_client
 * @property Partenaire $partenaire
 * @property Plat $plat
 * @property Client $client
 */
class Favori extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_partenaire', 'id_plat', 'id_client'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partenaire');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plat()
    {
        return $this->belongsTo('App\Plat', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'id_client');
    }
}
