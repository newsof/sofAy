<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_restaurant
 * @property int $id_plat
 * @property int $id_composant
 * @property int $id_famille
 * @property string $option
 * @property float $prix
 * @property string $statut
 * @property Partenaire $partenaire
 * @property Plat $plat
 * @property Composant $composant
 * @property FamillePlat $famillePlat
 */
class Option extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_restaurant', 'id_plat', 'id_composant', 'id_famille', 'option', 'prix', 'statut'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_restaurant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plat()
    {
        return $this->belongsTo('App\Plat', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function composant()
    {
        return $this->belongsTo('App\Composant', 'id_composant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function famillePlat()
    {
        return $this->belongsTo('App\FamillePlat', 'id_famille');
    }
}
