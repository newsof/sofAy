<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nom
 * @property string $northeast_lat
 * @property string $northeast_lng
 * @property string $southwest_lat
 * @property string $southwest_lng
 * @property boolean $is_active
 * @property boolean $is_block
 * @property Partenaire[] $partenaires
 * @property PartnerSector[] $partnerSectors
 */

class DeliverySector extends Model {
	/**
	 * @var array
	 */
	protected $fillable = ['nom', 'northeast_lat', 'northeast_lng', 'southwest_lat', 'southwest_lng', 'is_active', 'is_block'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function partenaires() {
		return $this->hasMany('App\Partenaire', 'id_sector');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function partnerSectors() {
		return $this->hasMany('App\PartnerSector', 'id_sector');
	}
}
