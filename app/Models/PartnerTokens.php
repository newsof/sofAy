<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerTokens extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_tokens';
	protected $dates      = [];

}
