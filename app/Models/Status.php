<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partenaire
 * @property string $nom
 * @property string $prenom
 * @property string $gsm
 * @property string $code
 * @property Partenaire $partenaire
 */

class Status extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'order_statuses';
	protected $dates      = [];

}
