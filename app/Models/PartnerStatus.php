<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerStatus extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_statuses';
	protected $dates      = [];

}
