<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_dish
 * @property int $id_partner
 * @property int $id_special_day
 * @property int $quantity
 * @property Plat $plat
 * @property Partenaire $partenaire
 * @property SpecialDay $specialDay
 */
class SpecialDayDetail extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'special_day_detail';

    /**
     * @var array
     */
    protected $fillable = ['id_dish', 'id_partner', 'id_special_day', 'quantity'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plat()
    {
        return $this->belongsTo('App\Plat', 'id_dish');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specialDay()
    {
        return $this->belongsTo('App\SpecialDay', 'id_special_day');
    }
}
