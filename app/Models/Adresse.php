<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_zone
 * @property int $id_city
 * @property string $adresse
 * @property string $title
 * @property string $rue
 * @property string $quartier
 * @property string $residence
 * @property string $bloc
 * @property string $ville
 * @property string $cp
 * @property string $cp2
 * @property string $pour
 * @property string $etat
 * @property string $latitude
 * @property string $longitude
 * @property string $crossStreet
 * @property string $apartment
 * @property string $municipality
 * @property string $comment
 * @property boolean $is_default
 * @property string $creation_date
 * @property string $update_date
 * @property Cp $Cp
 * @property Ville $Ville
 * @property Client[] $clients
 * @property Commande[] $commandes
 * @property Company[] $companies
 * @property CompanyAddress[] $companyAddresses
 */

class Adresse extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'adresses';
	protected $dates      = [];

	/**
	 * @var array
	 */
	protected $fillable = ['id_zone', 'id_city', 'adresse', 'title', 'rue', 'quartier', 'residence', 'bloc', 'ville', 'cp', 'cp2', 'pour', 'etat', 'latitude', 'longitude', 'crossStreet', 'apartment', 'municipality', 'comment', 'is_default', 'creation_date', 'update_date'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function cp() {
		return $this->belongsTo('App\Cp', 'id_zone');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function ville() {
		return $this->belongsTo('App\Ville', 'id_city');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function clients() {
		return $this->belongsToMany('App\Client', 'addressesforclient', 'id_address', 'id_client');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany

	public function commandes() {
	return $this->hasMany('App\Commande', 'id_address');
	}
	 */
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function companies() {
		return $this->hasMany('App\Company', 'id_address');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function companyAddresses() {
		return $this->hasMany('App\CompanyAddress', 'id_address');
	}
}
