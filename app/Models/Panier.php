<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partenaire
 * @property int $id_client
 * @property string $id_session
 * @property int $id_article
 * @property int $id_dimension
 * @property string $designation
 * @property string $dimension_title
 * @property int $qte
 * @property string $pu
 * @property string $pt
 * @property string $observ
 * @property string $etat
 * @property int $id_commande
 * @property string $date
 * @property string $date_commande
 * @property string $heure_commande
 * @property Partenaire $partenaire
 * @property Client $client
 * @property ReviewBasket[] $reviewBaskets
 */
class Panier extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'panier';

    /**
     * @var array
     */
    protected $fillable = ['id_partenaire', 'id_client', 'id_session', 'id_article', 'id_dimension', 'designation', 'dimension_title', 'qte', 'pu', 'pt', 'observ', 'etat', 'id_commande', 'date', 'date_commande', 'heure_commande'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partenaire');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviewBaskets()
    {
        return $this->hasMany('App\ReviewBasket', 'id_basket');
    }
}
