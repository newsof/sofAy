<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_ville
 * @property int $id_company
 * @property string $cp
 * @property string $slug
 * @property string $latitude
 * @property string $longitude
 * @property boolean $is_active
 * @property string $date
 * @property string $open_time
 * @property string $close_time
 * @property Adress[] $adresses
 * @property Affectrestocp[] $affectrestocps
 */
class Cp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cp';

    /**
     * @var array
     */
    protected $fillable = ['id_ville', 'id_company', 'cp', 'slug', 'latitude', 'longitude', 'is_active', 'date', 'open_time', 'close_time'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adresses()
    {
        return $this->hasMany('App\Adress', 'id_zone');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affectrestocps()
    {
        return $this->hasMany('App\Affectrestocp', 'Zip');
    }
}
