<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partner
 * @property int $id_dish_family
 * @property int $rank
 * @property Partenaire $partenaire
 */
class PartnerDishFamily extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'partner_dish_family';

    /**
     * @var array
     */
    protected $fillable = ['id_partner', 'id_dish_family', 'rank'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }
}
