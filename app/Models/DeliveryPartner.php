<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partner
 * @property int $id_order
 * @property string $id_order_patner
 * @property string $transport_type
 * @property float $weight
 * @property float $volume
 * @property float $shipping_cost
 * @property boolean $validate_payement
 * @property string $token_transaction
 * @property Partenaire $partenaire
 * @property Partenaire $partenaire
 * @property Commande $commande
 */
class DeliveryPartner extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'delivery_partner';

    /**
     * @var array
     */
    protected $fillable = ['id_partner', 'id_order', 'id_order_patner', 'transport_type', 'weight', 'volume', 'shipping_cost', 'validate_payement', 'token_transaction'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commande()
    {
        return $this->belongsTo('App\Commande', 'id_order');
    }
}
