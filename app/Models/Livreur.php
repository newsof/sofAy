<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_superviseur
 * @property int $id_partenaire
 * @property string $nom
 * @property string $prenom
 * @property string $photo
 * @property int $cp
 * @property string $tel
 * @property int $mobicash
 * @property string $email
 * @property string $login
 * @property string $password
 * @property string $commission
 * @property string $etat
 * @property boolean $is_connected
 * @property string $last_connection_date
 * @property integer $transport
 * @property boolean $is_active
 * @property User $utilisateur
 * @property Partenaire $partenaire
 * @property AVerserLivreur[] $aVerserLivreurs
 * @property Commande[] $commandes
 * @property DeliverymanPosition[] $deliverymanPositions
 * @property Recovery[] $recoveries
 */

class Livreur extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'deliverymen';
	protected $dates      = [];

	/**
	 * @var array
	 */
	protected $fillable = ['id_superviseur', 'id_partenaire', 'nom', 'prenom', 'photo', 'cp', 'tel', 'mobicash', 'email', 'login', 'password', 'commission', 'etat', 'is_connected', 'last_connection_date', 'transport', 'is_active'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function utilisateur() {
		return $this->belongsTo('App\User', 'id_superviseur');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function partenaire() {
		return $this->belongsTo('App\Partenaire', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function aVerserLivreurs() {
		return $this->hasMany('App\AVerserLivreur', 'id_livreur');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function commandes() {
		return $this->hasMany('App\Commande', 'id_livreur1');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function deliverymanPositions() {
		return $this->hasMany('App\DeliverymanPosition', 'id_deliveryman');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function recoveries() {
		return $this->hasMany('App\Recovery', 'id_deliveryman');
	}
}
