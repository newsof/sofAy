<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_p_boisson
 * @property int $id_partenaire
 * @property string $nom
 * @property string $prix
 * @property boolean $is_active
 * @property string $description
 * @property string $photo
 */
class Boisson extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'boisson';

    /**
     * @var array
     */
    protected $fillable = ['id_p_boisson', 'id_partenaire', 'nom', 'prix', 'is_active', 'description', 'photo'];

}
