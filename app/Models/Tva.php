<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $tva
 */
class Tva extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tva';

    /**
     * @var array
     */
    protected $fillable = ['tva'];

}
