<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerImages extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_images';
	protected $dates      = [];

	public function Partners() {
		return $this->belongsTo('App\Models\Partenaire', 'id_partner');
	}

}
