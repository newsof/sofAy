<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_restaurant
 * @property string $famille
 * @property string $slug
 * @property string $description
 * @property string $photo
 * @property string $statut
 * @property int $nbr_piece
 * @property int $ordre
 * @property Partenaire $partenaire
 * @property Composant[] $composants
 * @property Dimension[] $dimensions
 * @property Option[] $options
 * @property PRemise[] $pRemises
 * @property Plat[] $plats
 */
class FamillePlat extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'famille_plat';

    /**
     * @var array
     */
    protected $fillable = ['id_restaurant', 'famille', 'slug', 'description', 'photo', 'statut', 'nbr_piece', 'ordre'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_restaurant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function composants()
    {
        return $this->hasMany('App\Composant', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dimensions()
    {
        return $this->hasMany('App\Dimension', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('App\Option', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pRemises()
    {
        return $this->hasMany('App\PRemise', 'id_article_family');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plats()
    {
        return $this->hasMany('App\Plat', 'id_famille');
    }
}
