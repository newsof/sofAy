<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOptions extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'order_options';
	protected $dates      = [];

}
