<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_p
 * @property int $value
 * @property int $id_client
 * @property int $id_employe
 * @property int $id_entreprise
 */
class Vote extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'vote';

    /**
     * @var array
     */
    protected $fillable = ['id', 'id_p', 'value', 'id_client', 'id_employe', 'id_entreprise'];

}
