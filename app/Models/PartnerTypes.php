<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerTypes extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_types';
	protected $dates      = [];

}
