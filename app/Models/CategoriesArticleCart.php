<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_cart
 * @property int $id_article
 * @property int $id_component
 * @property string $component_title
 * @property int $id_option
 * @property string $option_title
 * @property string $option_price
 */
class CategoriesArticleCart extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'categories_article_cart';

    /**
     * @var array
     */
    protected $fillable = ['id_cart', 'id_article', 'id_component', 'component_title', 'id_option', 'option_title', 'option_price'];

}
