<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $zone
 * @property int $from_km
 * @property int $to_km
 */

class PaymentModes extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'payment_modes';
	protected $dates      = [];

}
