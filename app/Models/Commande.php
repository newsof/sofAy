<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_client
 * @property int $id_address
 * @property int $id_partenaire
 * @property int $id_livreur1
 * @property int $id_referral
 * @property string $code
 * @property boolean $order_type
 * @property string $date_envoie
 * @property string $heure_envoie
 * @property string $pour_le
 * @property string $tempsestime
 * @property string $id_language
 * @property string $code_promo
 * @property string $etat
 * @property string $tarif
 * @property string $sous_total
 * @property string $tva
 * @property string $total
 * @property string $balance_consumed
 * @property string $balance
 * @property string $etat_paiement
 * @property string $mode_paiement
 * @property string $etat_pay_resto
 * @property int $id_cashier
 * @property string $pay_resto_date
 * @property string $etat_resto
 * @property string $date_heure_refus
 * @property string $refuse_reason
 * @property string $conf
 * @property string $conf_print
 * @property string $agent
 * @property string $android
 * @property string $type
 * @property string $statut_mail_notation
 * @property string $mode_contact_client
 * @property string $application_type
 * @property int $id_promo_campaign
 * @property boolean $is_awaiting
 * @property integer $id_option
 * @property int $montant_verser
 * @property string $token
 * @property int $etat_process
 * @property int $etat_response
 * @property int $review_status
 * @property string $review_answer_date
 * @property string $supp_frais
 * @property boolean $new_version
 * @property int $signal_cmd
 * @property string $description
 * @property boolean $payment_delivery
 * @property boolean $confirm_payment_delivery
 * @property string $remain_topay
 * @property boolean $Is_order_paid_cashier
 * @property boolean $is_order_paid_deliveryman
 * @property int $transfer_mobicach
 * @property Client $client
 * @property Adress $adress
 * @property Partenaire $partenaire
 * @property Livreur $livreur
 * @property Referral $referral
 * @property Client[] $clients
 * @property DeliveryPartner[] $deliveryPartners
 * @property MobicashTransaction[] $mobicashTransactions
 * @property Referral[] $referrals
 * @property Review[] $reviews
 * @property Transaction $transaction
 */

class Commande extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'orders';
	protected $dates      = [];
	/**
	 * @var array
	 */
	/*
	protected $fillable = ['id_client', 'id_address', 'id_partenaire', 'id_livreur1', 'id_referral', 'code', 'order_type', 'date_envoie', 'heure_envoie', 'pour_le', 'tempsestime', 'id_language', 'code_promo', 'etat', 'tarif', 'sous_total', 'tva', 'total', 'balance_consumed', 'balance', 'etat_paiement', 'mode_paiement', 'etat_pay_resto', 'id_cashier', 'pay_resto_date', 'etat_resto', 'date_heure_refus', 'refuse_reason', 'conf', 'conf_print', 'agent', 'android', 'type', 'statut_mail_notation', 'mode_contact_client', 'application_type', 'id_promo_campaign', 'is_awaiting', 'id_option', 'montant_verser', 'token', 'etat_process', 'etat_response', 'review_status', 'review_answer_date', 'supp_frais', 'new_version', 'signal_cmd', 'description', 'payment_delivery', 'confirm_payment_delivery', 'remain_topay', 'Is_order_paid_cashier', 'is_order_paid_deliveryman', 'transfer_mobicach'];
	 */
	// !!!!!!!!!!!!!!!!!!!!!!! id_language ??? no language table found !!!!!!!!
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function client() {
		return $this->hasOne('App\Models\Client', 'id', 'id_client');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	//!!!!!!!!!!!! Not found in database !!!!!!!!!!!!
	public function adress() {
		return $this->belongsTo('App\Models\Adresse', 'id_address');
	}
	//!!!!!!!!!!!!!!!!!!!!!!

	public function adressA() {
		return $this->hasOne('App\Models\Adresse', 'id', 'id_addressA');
	}

	public function adressB() {
		return $this->hasOne('App\Models\Adresse', 'id', 'id_addressB');
	}

	public function status() {
		return $this->hasOne('App\Models\Status', 'id', 'id_order_status');
	}

	public function PaymentMode() {
		return $this->hasOne('App\Models\PaymentModes', 'id', 'id_payment_mode');
	}

	public function orderType() {
		return $this->hasOne('App\Models\OrderTypes', 'id', 'id_order_type');
	}

	public function orderOrigine() {
		return $this->hasOne('App\Models\OrderOrigine', 'id', 'id_order_origine');
	}

	public function delivery_time() {
		return $this->hasOne('App\Models\DeliveryTime', 'id', 'id_delivery_time');
	}

	public function orderOptions() {
		return $this->hasOne('App\Models\OrderOptions', 'id', 'id_order_option');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function partenaire() {
		return $this->hasOne('App\Models\Partenaire', 'id', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function livreur() {
		return $this->hasOne('App\Models\Livreur', 'id', 'id_deliveryman');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function referral() {
		return $this->belongsTo('App\Models\Referral', 'id_referral');
	}

	public function cashier() {
		return $this->belongsTo('App\Models\Caissier', 'id_cashier');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function clients() {
		return $this->hasMany('App\Client', 'id_ref_order');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function deliveryPartners() {
		return $this->hasMany('App\DeliveryPartner', 'id_order');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function mobicashTransactions() {
		return $this->hasMany('App\MobicashTransaction', 'id_order');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function referrals() {
		return $this->hasMany('App\Referral', 'id_referralOrder');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function reviews() {
		return $this->hasMany('App\Review', 'id_order');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function transaction() {
		return $this->hasOne('App\Transaction', 'id_order');
	}
}
