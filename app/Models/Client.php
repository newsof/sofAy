<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_ref_order
 * @property int $id_company
 * @property boolean $is_referral
 * @property string $nom
 * @property string $prenom
 * @property string $tel
 * @property string $gsm
 * @property string $extension
 * @property string $email
 * @property string $commentaire
 * @property string $login
 * @property string $password
 * @property string $conf
 * @property boolean $is_banned
 * @property int $session
 * @property string $date
 * @property string $civ
 * @property string $satisfaction
 * @property boolean $is_corporate
 * @property boolean $is_pro
 * @property string $balance
 * @property boolean $accept_promo
 * @property boolean $is_connected
 * @property string $last_connection_date
 * @property string $id_fb
 * @property string $id_twitter
 * @property string $client_token
 * @property string $referral_code
 * @property int $conventionne
 * @property string $tax_registration_nb
 * @property Commande $commande
 * @property Company $company
 * @property Adress[] $adresses
 * @property ClientCode[] $clientCodes
 * @property Commande[] $commandes
 * @property CompanyReqBalance[] $companyReqBalances
 * @property Favori[] $favoris
 * @property Panier[] $paniers
 * @property Promotion[] $promotions
 * @property Referral[] $referrals
 */
class Client extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'client';

    /**
     * @var array
     */
    protected $fillable = ['id_ref_order', 'id_company', 'is_referral', 'nom', 'prenom', 'tel', 'gsm', 'extension', 'email', 'commentaire', 'login', 'password', 'conf', 'is_banned', 'session', 'date', 'civ', 'satisfaction', 'is_corporate', 'is_pro', 'balance', 'accept_promo', 'is_connected', 'last_connection_date', 'id_fb', 'id_twitter', 'client_token', 'referral_code', 'conventionne', 'tax_registration_nb'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commande()
    {
        return $this->belongsTo('App\Commande', 'id_ref_order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Company', 'id_company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function adresses()
    {
        return $this->belongsToMany('App\Adress', 'addressesforclient', 'id_client', 'id_address');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientCodes()
    {
        return $this->hasMany('App\ClientCode', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commandes()
    {
        return $this->hasMany('App\Commande', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyReqBalances()
    {
        return $this->hasMany('App\CompanyReqBalance', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favoris()
    {
        return $this->hasMany('App\Favori', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paniers()
    {
        return $this->hasMany('App\Panier', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotions()
    {
        return $this->hasMany('App\Promotion', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referrals()
    {
        return $this->hasMany('App\Referral', 'id_client');
    }
}
