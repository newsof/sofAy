<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_part
 * @property string $jour_conge
 * @property boolean $is_active
 * @property string $date
 * @property Partenaire $partenaire
 */
class Conge extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'conge';

    /**
     * @var array
     */
    protected $fillable = ['id_part', 'jour_conge', 'is_active', 'date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_part');
    }
}
