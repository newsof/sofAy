<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nom
 * @property string $northeast_lat
 * @property string $northeast_lng
 * @property string $southwest_lat
 * @property string $southwest_lng
 * @property boolean $is_active
 * @property boolean $is_block
 * @property Partenaire[] $partenaires
 * @property PartnerSector[] $partnerSectors
 */

class PartnerCategory extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_categories';
	protected $dates      = [];

}
