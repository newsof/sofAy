<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryTime extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'delivery_times';
	protected $dates      = [];

}
