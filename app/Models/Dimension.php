<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_restaurant
 * @property int $id_famille
 * @property int $id_plat
 * @property string $dimension
 * @property string $prix_particulier
 * @property string $prix_resto
 * @property string $prix
 * @property boolean $is_active
 * @property Partenaire $partenaire
 * @property FamillePlat $famillePlat
 * @property Plat $plat
 */
class Dimension extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_restaurant', 'id_famille', 'id_plat', 'dimension', 'prix_particulier', 'prix_resto', 'prix', 'is_active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_restaurant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function famillePlat()
    {
        return $this->belongsTo('App\FamillePlat', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plat()
    {
        return $this->belongsTo('App\Plat', 'id_plat');
    }
}
