<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $label
 * @property string $slug
 * @property string $description
 * @property string $created_on
 * @property string $updated_on
 * @property string $status
 */
class ProductCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * @var array
     */
    protected $fillable = ['title', 'slug', 'description', 'date_creation', 'date_update', 'status'];


    public function parent()
    {

        return $this->hasOne('App\Models\ProductCategory', 'id', 'id_parent')->orderBy('display_order');

    }

    public function children()
    {

        return $this->hasMany('App\Models\ProductCategory', 'id_parent', 'id')->orderBy('display_order');

    }

    public static function tree()
    {

        return static::with(implode('.', array_fill(0, 100, 'children')))->where('id_parent', '=', '0')->orderBy('display_order')->get();

    }

    public function getAll()
    {
        return $this->all()->toArray();
    }

    public function getAllByCat($cat)
    {
        return static::with(implode('.', array_fill(0, 100, 'children')))->where('id_parent', '=', $cat)->orderBy('display_order')->get();
    }


    public function getAllByPartner($cat)
    {
        return static::where('id_partner', '=', $cat)->orderBy('display_order')->get();
    }

}
