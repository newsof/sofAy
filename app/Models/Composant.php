<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_restaurant
 * @property int $id_plat
 * @property int $id_famille
 * @property string $composant
 * @property string $nbr_max_choix
 * @property int $nbr_options
 * @property boolean $is_active
 * @property Partenaire $partenaire
 * @property Plat $plat
 * @property FamillePlat $famillePlat
 * @property Option[] $options
 */
class Composant extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_restaurant', 'id_plat', 'id_famille', 'composant', 'nbr_max_choix', 'nbr_options', 'is_active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_restaurant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plat()
    {
        return $this->belongsTo('App\Plat', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function famillePlat()
    {
        return $this->belongsTo('App\FamillePlat', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('App\Option', 'id_composant');
    }
}
