<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerHoliday extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_holidays';
	protected $dates      = [];

	public function Partners() {
		return $this->belongsTo('App\Models\Partenaire', 'id_partner');
	}

}
