<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_address
 * @property int $code
 * @property string $title
 * @property string $description
 * @property string $photo
 * @property string $photo_cover
 * @property string $token
 * @property string $creation_date
 * @property boolean $is_active
 * @property string $tax_registration_nb
 * @property Adress $adress
 * @property Client[] $clients
 * @property CompanyAddress[] $companyAddresses
 * @property CompanyReqBalance[] $companyReqBalances
 * @property Recovery[] $recoveries
 * @property SpecialDay[] $specialDays
 */
class Companie extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_address', 'code', 'title', 'description', 'photo', 'photo_cover', 'token', 'creation_date', 'is_active', 'tax_registration_nb'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adress()
    {
        return $this->belongsTo('App\Adress', 'id_address');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients()
    {
        return $this->hasMany('App\Client', 'id_company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyAddresses()
    {
        return $this->hasMany('App\CompanyAddress', 'id_company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyReqBalances()
    {
        return $this->hasMany('App\CompanyReqBalance', 'id_company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recoveries()
    {
        return $this->hasMany('App\Recovery', 'id_company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialDays()
    {
        return $this->hasMany('App\SpecialDay', 'id_company');
    }
}
