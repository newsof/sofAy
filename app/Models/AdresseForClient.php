<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_client
 * @property int $id_address
 * @property Client $client
 * @property Adress $adress
 */
class AdresseForClient extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'addressesforclient';

    /**
     * @var array
     */
    protected $fillable = ['id_client', 'id_address'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adress()
    {
        return $this->belongsTo('App\Adress', 'id_address');
    }
}
