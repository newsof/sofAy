<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_restaurant
 * @property int $id_famille
 * @property string $nom
 * @property string $tva
 * @property string $prix
 * @property string $prix_particulier
 * @property string $prix_resto
 * @property string $statut
 * @property string $description
 * @property string $photo
 * @property Partenaire $partenaire
 * @property FamillePlat $famillePlat
 * @property Composant[] $composants
 * @property Dimension[] $dimensions
 * @property Favori[] $favoris
 * @property Option[] $options
 * @property PRemise[] $pRemises
 * @property SpecialDayDetail[] $specialDayDetails
 */
class Plat extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_restaurant', 'id_famille', 'nom', 'tva', 'prix', 'prix_particulier', 'prix_resto', 'statut', 'description', 'photo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_restaurant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function famillePlat()
    {
        return $this->belongsTo('App\FamillePlat', 'id_famille');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function composants()
    {
        return $this->hasMany('App\Composant', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dimensions()
    {
        return $this->hasMany('App\Dimension', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favoris()
    {
        return $this->hasMany('App\Favori', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany('App\Option', 'id_plat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pRemises()
    {
        return $this->hasMany('App\PRemise', 'id_article');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialDayDetails()
    {
        return $this->hasMany('App\SpecialDayDetail', 'id_dish');
    }
}
