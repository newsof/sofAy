<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $zone
 * @property int $from_km
 * @property int $to_km
 */

class Zonning extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'zonings';
	protected $dates      = [];

	/**
	 * @var array
	 */
	protected $fillable = ['zone', 'from_km', 'to_km'];

}
