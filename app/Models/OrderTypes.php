<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTypes extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'order_types';
	protected $dates      = [];

}
