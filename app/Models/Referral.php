<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model {

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'referrals';
	protected $dates      = [];
}
