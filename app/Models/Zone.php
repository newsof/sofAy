<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'zones';
	protected $dates      = [];

	public function Ville() {
		return $this->belongsTo('App\Models\Ville', 'id_ville');
	}

}
