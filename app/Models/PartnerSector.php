<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partner
 * @property int $id_sector
 * @property Partenaire $partenaire
 * @property DeliverySector $deliverySector
 */
class PartnerSector extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_partner', 'id_sector'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'id_partner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliverySector()
    {
        return $this->belongsTo('App\DeliverySector', 'id_sector');
    }
}
