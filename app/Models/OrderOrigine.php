<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOrigine extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'order_origines';
	protected $dates      = [];

}
