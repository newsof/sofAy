<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $PartID
 * @property int $Zip
 * @property boolean $is_active
 * @property int $delai_livraison_min
 * @property int $delai_livraison_max
 * @property float $min_charge
 * @property float $frais_livraison
 * @property Partenaire $partenaire
 * @property Cp $cp
 */
class Affectrestocp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'affectrestocp';

    /**
     * @var array
     */
    protected $fillable = ['PartID', 'Zip', 'is_active', 'delai_livraison_min', 'delai_livraison_max', 'min_charge', 'frais_livraison'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partenaire()
    {
        return $this->belongsTo('App\Partenaire', 'PartID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cp()
    {
        return $this->belongsTo('App\Cp', 'Zip');
    }
}
