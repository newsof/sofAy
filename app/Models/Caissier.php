<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_partenaire
 * @property string $nom
 * @property string $prenom
 * @property string $gsm
 * @property string $code
 * @property Partenaire $partenaire
 */

class Caissier extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'cashiers';
	protected $dates      = [];

	/**
	 * @var array
	 */
	//protected $fillable = ['id_partenaire', 'nom', 'prenom', 'gsm', 'code'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function partenaire() {
		return $this->belongsTo('App\Partenaire', 'id_partenaire');
	}
}
