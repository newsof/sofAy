<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_sector
 * @property string $delivery_price
 * @property int $sac_number
 * @property string $nom
 * @property string $slug
 * @property string $photo
 * @property string $photo2
 * @property int $id_famille
 * @property string $fax
 * @property string $tel
 * @property int $cp
 * @property string $cp2
 * @property string $adresse
 * @property string $lat
 * @property string $long
 * @property string $gsm
 * @property string $email
 * @property int $min_charge
 * @property string $min_charge2
 * @property int $id_ville
 * @property string $de_matin
 * @property string $a_matin
 * @property string $de_soir
 * @property string $a_soir
 * @property string $statut
 * @property string $date
 * @property string $etat
 * @property string $etat_occupe_particulier
 * @property string $rubrique
 * @property int $ordre
 * @property string $newsletter
 * @property string $preparation
 * @property string $description
 * @property string $delai_emporte
 * @property boolean $conf_admin
 * @property int $delai_emporte_min
 * @property int $delai_emporte_max
 * @property string $jours_travail
 * @property int $type_livraison
 * @property string $code_resto
 * @property boolean $is_streetfood
 * @property string $type
 * @property float $taux_remise
 * @property boolean $is_new
 * @property string $new_begin_date
 * @property string $new_end_date
 * @property int $display_order
 * @property string $tax_registration_nb
 * @property DeliverySector $deliverySector
 * @property Affectrestocp[] $affectrestocps
 * @property Caissier[] $caissiers
 * @property Commande[] $commandes
 * @property Composant[] $composants
 * @property Conge[] $conges
 * @property DeliveryPartner[] $deliveryPartners
 * @property Dimension[] $dimensions
 * @property FamillePartenaire[] $famillePartenaires
 * @property FamillePlat[] $famillePlats
 * @property Favori[] $favoris
 * @property Livreur[] $livreurs
 * @property Option[] $options
 * @property PRemise[] $pRemises
 * @property Panier[] $paniers
 * @property PartnerDishFamily[] $partnerDishFamilies
 * @property PartnerSector[] $partnerSectors
 * @property Plat[] $plats
 * @property PromotionCampaign[] $promotionCampaigns
 * @property Promotion[] $promotions
 * @property SpecialDayDetail[] $specialDayDetails
 * @property TauxRemise[] $tauxRemises
 * @property User[] $utilisateurs
 */

class Partenaire extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */

	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partners';
	protected $dates      = [];

	/**
	 * @var array
	 */
	protected $fillable = ['id_sector', 'delivery_price', 'sac_number', 'nom', 'slug', 'photo', 'photo2', 'id_famille', 'fax', 'tel', 'cp', 'cp2', 'adresse', 'lat', 'long', 'gsm', 'email', 'min_charge', 'min_charge2', 'id_ville', 'de_matin', 'a_matin', 'de_soir', 'a_soir', 'statut', 'date', 'etat', 'etat_occupe_particulier', 'rubrique', 'ordre', 'newsletter', 'preparation', 'description', 'delai_emporte', 'conf_admin', 'delai_emporte_min', 'delai_emporte_max', 'jours_travail', 'type_livraison', 'code_resto', 'is_streetfood', 'type', 'taux_remise', 'is_new', 'new_begin_date', 'new_end_date', 'display_order', 'tax_registration_nb'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function deliverySector() {
		return $this->hasOne('App\Models\DeliverySector', 'id', 'id_sector');
	}

	public function Type() {
		return $this->hasOne('App\Models\PartnerTypes', 'id', 'id_partner_type');
	}

	public function Category() {
		return $this->hasOne('App\Models\PartnerCategory', 'id', 'id_partner_category');
	}

	public function Status() {
		return $this->hasOne('App\Models\PartnerStatus', 'id', 'id_partner_status');
	}
	public function Zone() {
		return $this->hasOne('App\Models\Zone', 'id', 'id_zone');
	}
	public function Ville() {
		return $this->hasOne('App\Models\Ville', 'id', 'id_city');
	}
	public function Commissions() {
		return $this->hasMany('App\Models\PartnerCommissions', 'id_partner');
	}
	public function Discounts() {
		return $this->hasMany('App\Models\PartnerDiscounts', 'id_partner');
	}
	public function PartnerFamilies() {
		return $this->hasMany('App\Models\PartnerFamily', 'id_partner');
	}
	public function PartnerHoliday() {
		return $this->hasMany('App\Models\PartnerHoliday', 'id_partner');
	}
	public function PartnerImages() {
		return $this->hasMany('App\Models\PartnerImages', 'id_partner');
	}

	public function PartnerPaymentModes() {
		return $this->belongsToMany('App\Models\PaymentModes', 'partner_payment_modes', 'id_partner', 'id_payment_mode');
	}

	public function ProductCategory() {
		return $this->belongsToMany('App\Models\ProductCategory', 'partner_product_categories', 'id_partner', 'id_product_category')->withPivot('rank')
		;
	}
	public function Sectors() {
		return $this->belongsToMany('App\Models\DeliverySector', 'partner_sectors', 'id_partner', 'id_sector')->withPivot('is_out_sector');
		;
	}
	public function Affiliate() {
		return $this->belongsToMany('App\Models\Partenaire', 'partner_subsidiaries', 'id_partenaire', 'id_affiliate');
	}
	public function WorkingDays() {
		return $this->belongsToMany('App\Models\WorkingDay', 'partner_workingdays', 'id_partner', 'id_workingday')->withPivot('am_begin', 'am_end', 'pm_begin', 'pm_end', 'is_active');
	}
	public function Zones() {
		return $this->belongsToMany('App\Models\Zone', 'partner_zones', 'id_partner', 'id_zone')->withPivot('is_active', 'delai_livraison_min', 'delai_livraison_max', 'min_charge', 'frais_livraison');
	}
	public function Zonning() {
		return $this->belongsToMany('App\Models\Zonning', 'partner_zonings', 'id_partner', 'id_zoning')->withPivot('price');
	}

	public function PartnerTokens() {
		return $this->hasMany('App\Models\PartnerTokens', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function affectrestocps() {
		return $this->hasMany('App\Affectrestocp', 'PartID');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function caissiers() {
		return $this->hasMany('App\Caissier', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function commandes() {
		return $this->hasMany('App\Commande', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function composants() {
		return $this->hasMany('App\Composant', 'id_restaurant');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function conges() {
		return $this->hasMany('App\Conge', 'id_part');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function deliveryPartners() {
		return $this->hasMany('App\DeliveryPartner', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function dimensions() {
		return $this->hasMany('App\Dimension', 'id_restaurant');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function famillePartenaires() {
		return $this->hasMany('App\FamillePartenaire', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function famillePlats() {
		return $this->hasMany('App\FamillePlat', 'id_restaurant');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function favoris() {
		return $this->hasMany('App\Favori', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function livreurs() {
		return $this->hasMany('App\Livreur', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function options() {
		return $this->hasMany('App\Option', 'id_restaurant');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function pRemises() {
		return $this->hasMany('App\PRemise', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function paniers() {
		return $this->hasMany('App\Panier', 'id_partenaire');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function partnerDishFamilies() {
		return $this->hasMany('App\PartnerDishFamily', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function partnerSectors() {
		return $this->hasMany('App\PartnerSector', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function plats() {
		return $this->hasMany('App\Plat', 'id_restaurant');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function promotionCampaigns() {
		return $this->hasMany('App\PromotionCampaign', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function promotions() {
		return $this->hasMany('App\Promotion', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function specialDayDetails() {
		return $this->hasMany('App\SpecialDayDetail', 'id_partner');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function tauxRemises() {
		return $this->hasMany('App\TauxRemise');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function utilisateurs() {
		return $this->hasMany('App\User', 'id_partenaire');
	}
}
