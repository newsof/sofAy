<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nom
 * @property string $statut
 * @property string $slug
 * @property Adress[] $adresses
 */

class PartnerPaymentModes extends Model {
	protected $primaryKey = 'id';
	public $incrementing  = true;
	protected $table      = 'partner_payment_modes';
	protected $dates      = [];

	public function adresses() {
		return $this->hasMany('App\Adress', 'id_city');
	}
}
